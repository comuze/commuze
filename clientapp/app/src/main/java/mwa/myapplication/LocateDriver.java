package mwa.myapplication;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;


public class LocateDriver extends FragmentActivity {
    private GoogleMap  mMap;
    public static final String DEBUG_TAG = "DriverService"; // Debug TAG

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locate_driver);
        Bundle extras = getIntent().getExtras();
        setUpMapIfNeeded();
		TextView textView = (TextView)findViewById(R.id.textView);

        TextView text_driver = (TextView)findViewById(R.id.textView2);
       // TextView text_vehhum = (TextView)findViewById(R.id.textView3);
       // TextView text_telno = (TextView)findViewById(R.id.textView4);
		
        String payload = extras.getString("STRING_I_NEED");

        try {
            JSONObject json = new JSONObject(payload);
            String driver = json.getString("driver");
            String telno = json.getString("Telno");
            String vehnum = json.getString("Vehnum");

            text_driver.setText(driver +"\n");
            text_driver.append(vehnum + "\n");
            text_driver.append(telno);
            //text_vehhum.setText(vehnum);
           // text_telno.setText(telno);
			
			Log.i(DEBUG_TAG,"  driver:\t" + driver +
                "  telno:\t" + telno +
                "  vehnum:\t" + vehnum);	
				
				
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.locate_driver, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.

        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {

        LatLng originpoint = new LatLng(12.873810, 77.743110);
        mMap.addMarker(new MarkerOptions().position(originpoint).title("Your Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(originpoint,11));
        Bundle extras = getIntent().getExtras();
        String lat = extras.getString("LAT");
        String lng = extras.getString("LONG");
        Log.i("Maps", " Drvr Coordinates:" + lat.toString() + ", " + lng.toString());
        LatLng drvrpoint = new LatLng(Float.parseFloat(lat),Float.parseFloat(lng));
        mMap.addMarker(new MarkerOptions().position(drvrpoint).title("Drvr Location"));

    }


}
